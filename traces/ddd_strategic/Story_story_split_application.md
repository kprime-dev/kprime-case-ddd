# Story GOAL story_split_application
+ goal 1614348282396
+ termbase ddd_strategic_db.xml
+ actor Architect

Bounded Contexts and Subdomains exist at different levels.

A Subdomain is a portion of the problem space, it's a natural partitioning of the system, often reflecting the structure of the organisation. So logistics and operations might be separated from invoicing & billing. Eric differentiates core, supporting and generic subdomains according to their business relevance in the given scenario.

Bounded_Contexts are portions of the solutions space. They're models. It would be a good thing to have them, reflect the domains-subdomains partitioning ...but life isn't always that easy. And you might have bloated legacy domain encompassing everything, or more context in the same subdomain (i.e. old legacy app the replacement app somebody is building).
---
To have a Bounded Context you need to have a model, and an explicit boundary around it. Exactly what's missing in many data-driven application that use databases to share data.

Another - orthogonal - way to see it may be the following. Ubiquitous Language, the special condition where every term has a single unambiguous definition, doesn't scale. The more you enlarge it, the more ambiguity creeps in. If you want to achieve precise, unambiguous models, you need to make their boundaries explicit, and speak many little Ubiquitous_Languages, each one within a single Bounded Context, with a well defined purpose.
